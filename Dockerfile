# this is our first build stage, it will not persist in the final image
FROM registry.hzdr.de/hzb/epics/base/ubuntu_20_04:1.1.0 as intermediate

RUN mkdir -p /opt/epics/support
RUN mkdir -p /opt/epics/ioc

# install git
RUN apt-get update
RUN apt-get install -y git

# add credentials on build
ARG SSH_PRIVATE_KEY
RUN mkdir /root/.ssh/
RUN echo "${SSH_PRIVATE_KEY}" > /root/.ssh/id_rsa
RUN chmod 600 /root/.ssh/id_rsa

# make sure your domain is accepted
RUN touch /root/.ssh/known_hosts
RUN ssh-keyscan codebase.helmholtz.cloud >> /root/.ssh/known_hosts

# This step is not important for deployment. We could use HTTPS since the repo is 
# public. However, later if we want to modify this repo, it gives us write access 
# if our dev_container has ssh with write access configured in the host.
RUN git clone --depth 1 --branch 1-0-0 git@codebase.helmholtz.cloud:hzb/epics/ioc/source/simmotoriocsource.git ${IOC}/motorMotorSim

## Now create the image we will actually use in the end
FROM registry.hzdr.de/hzb/epics/base/ubuntu_20_04:1.1.0 

RUN mkdir -p /opt/epics/support
RUN mkdir -p /opt/epics/ioc

# configure environment
COPY RELEASE.local ${SUPPORT}/RELEASE.local
# A different location is required by the motor ioc
COPY RELEASE.local ${IOC}/RELEASE.linux-x86_64.local
COPY RELEASE.local ${IOC}/RELEASE.local

# install autosave 
RUN git clone --depth 1 --recursive --branch R5-10-2 https://github.com/epics-modules/autosave.git ${SUPPORT}/autosave
RUN make -C ${SUPPORT}/autosave -j $(nproc)

# install seq
RUN git clone --depth 1 --recursive --branch vendor_2_2_8 https://github.com/ISISComputingGroup/EPICS-seq.git ${SUPPORT}/seq
RUN make -C ${SUPPORT}/seq -j $(nproc)

# install sscan
RUN git clone --depth 1 --recursive --branch R2-11-5 https://github.com/epics-modules/sscan.git ${SUPPORT}/sscan
RUN make -C ${SUPPORT}/sscan -j $(nproc)

# install calc
RUN git clone --depth 1 --recursive --branch R3-7-4 https://github.com/epics-modules/calc.git ${SUPPORT}/calc
RUN make -C ${SUPPORT}/calc -j $(nproc)

# install asyn
RUN git clone --depth 1 --recursive --branch R4-44-2 https://github.com/epics-modules/asyn.git ${SUPPORT}/asyn
RUN make -C ${SUPPORT}/asyn -j $(nproc)

# install busy
RUN git clone --depth 1 --recursive --branch R1-7-4 https://github.com/epics-modules/busy.git ${SUPPORT}/busy
RUN make -C ${SUPPORT}/busy -j $(nproc)

# install ipac
RUN git clone --depth 1 --recursive --branch 2.16 https://github.com/epics-modules/ipac.git ${SUPPORT}/ipac
RUN make -C ${SUPPORT}/ipac -j $(nproc)

# install the motor support module
RUN git clone --depth 1 --branch R7-3-1 https://github.com/epics-modules/motor ${SUPPORT}/motor
RUN make -C ${SUPPORT}/motor -j $(nproc)

# install the sim motor ioc
COPY --from=intermediate ${IOC}/motorMotorSim ${IOC}/motorMotorSim
RUN make -C ${IOC}/motorMotorSim -j $(nproc)
